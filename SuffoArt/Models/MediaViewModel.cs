﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace SuffoArt.Models
{
    public class MediaViewModel
    {
        [Required]
        public int Tipo { get; set; }
        [Required]
        public string Titulo { get; set; }
        
        public string Descripcion { get; set; }
        [DataType(DataType.Upload)]
        public HttpPostedFileBase Ubicacion { get; set; }
    }
}