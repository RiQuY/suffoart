﻿using SuffoArt.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace SuffoArt.Controllers
{
    public class AdminController : Controller
    {
        // Instanciación del modelo
        LoginBD loginModel = new LoginBD();
        MediaBD mediaModel = new MediaBD();


        // GET: Admin
        public ActionResult IniciarSesion()
        {
            if (Session["username"] == null)
            {
                return View();
            }
            else
            {
                Session["username"] = null;
                return View();
            }
        }

        // POST: IniciarSesion
        [HttpPost]
        public ActionResult IniciarSesion(LoginViewModel datos)
        {
            bool error = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (loginModel.ComprobarLogin(datos))
                    {
                        Session["username"] = "Login Correcto";
                    }
                    else
                    {
                        error = true;
                    }
                }
                else
                {
                    error = true;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                error = true;
            }

            if (error)
            {
                return RedirectToAction("Error");
            }
            else
            {
                return RedirectToAction("PanelDeControl");
            }
        }

        public ActionResult PanelDeControl()
        {
            if (Session["username"] == null)
            {
                return RedirectToAction("IniciarSesion");
            }
            else
            {
                return View();
            }
        }

        //public ActionResult AdministrarBlog()
        //{
        //    if (Session["username"] == null)
        //    {
        //        return RedirectToAction("IniciarSesion");
        //    }
        //    else
        //    {
        //        return View();
        //    }
        //}

        public ActionResult AdministrarAnimaciones()
        {
            if (Session["username"] != null)
            {
                if (Session["NumRegistroAnimaciones"] == null)
                {
                    Session["NumRegistroAnimaciones"] = 0;
                }
                List<Media> animaciones = mediaModel.GetAnimacionesPaginated((int)Session["NumRegistroAnimaciones"]);

                return View(animaciones);
            }
            else
            {
                return RedirectToAction("IniciarSesion");
            }
        }

        // GET: ResetNumPaginaAdminAnimaciones()
        public ActionResult ResetNumPaginaAdminAnimaciones()
        {
            Session["NumRegistroAnimaciones"] = null;
            return RedirectToAction("AdministrarAnimaciones");
        }

        // GET: SiguientePaginaAdminAnimaciones()
        public ActionResult SiguientePaginaAdminAnimaciones()
        {
            if (Session["NumRegistroAnimaciones"] == null)
            {
                Session["NumRegistroAnimaciones"] = 0;
            }
            int numRegistro = (int)Session["NumRegistroAnimaciones"];
            numRegistro += 2;
            Session["NumRegistroAnimaciones"] = numRegistro;
            return RedirectToAction("AdministrarAnimaciones");
        }

        public ActionResult AdministrarIlustraciones()
        {
            if (Session["username"] != null)
            {
                if (Session["NumRegistroIlustraciones"] == null)
                {
                    Session["NumRegistroIlustraciones"] = 0;
                }
                List<Media> ilustraciones = mediaModel.GetIlustracionesPaginated((int)Session["NumRegistroIlustraciones"]);

                return View(ilustraciones);
            }
            else
            {
                return RedirectToAction("IniciarSesion");
            }
        }

        // GET: ResetNumPaginaAdminIlustraciones()
        public ActionResult ResetNumPaginaAdminIlustraciones()
        {
            Session["NumRegistroIlustraciones"] = null;
            return RedirectToAction("AdministrarIlustraciones");
        }

        // GET: SiguientePaginaAdminIlustraciones()
        public ActionResult SiguientePaginaAdminIlustraciones()
        {
            if (Session["NumRegistroIlustraciones"] == null)
            {
                Session["NumRegistroIlustraciones"] = 0;
            }
            int numRegistro = (int)Session["NumRegistroIlustraciones"];
            numRegistro += 8;
            Session["NumRegistroIlustraciones"] = numRegistro;
            return RedirectToAction("AdministrarIlustraciones");
        }

        public ActionResult AñadirAnimacionesoIlustraciones()
        {
            if (Session["username"] == null)
            {
                return RedirectToAction("IniciarSesion");
            }
            else
            {
                return View();
            }
        }

        [HttpPost]
        public ActionResult AñadirAnimacionesoIlustraciones(MediaViewModel datos)
        {
            bool error = false;
            try
            {
                if (ModelState.IsValid)
                {
                    if (mediaModel.AñadirMedia(datos))
                    {
                        HttpPostedFileBase file = datos.Ubicacion;

                        if (file.ContentLength > 0 && file.ContentLength <= 41943000)
                        {
                            var fileName = Path.GetFileName(file.FileName);
                            if (datos.Tipo == 1)
                            {
                                var path = Path.Combine(Server.MapPath("~/Recursos/uploads/Ilustraciones"), fileName);
                                file.SaveAs(path);
                            }
                            else if (datos.Tipo == 2)
                            {
                                var path = Path.Combine(Server.MapPath("~/Recursos/uploads/Animaciones"), fileName);
                                file.SaveAs(path);
                            }
                        }
                    }
                    else
                    {
                        error = true;
                    }
                }
                else
                {
                    error = true;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                error = true;
            }

            if (error)
            {
                return RedirectToAction("Error");
            }
            else
            {
                return RedirectToAction("PanelDeControl");
            }
        }

        public ActionResult EditarAnimacion(int id)
        {
            if (Session["username"] != null)
            {
                Media animacion = mediaModel.GetMedia(id);
                return View(animacion);
            }
            else
            {
                return RedirectToAction("IniciarSesion");
            }
        }

        public ActionResult EditarIlustracion(int id)
        {
            if (Session["username"] != null)
            {
                Media ilustracion = mediaModel.GetMedia(id);
                return View(ilustracion);
            }
            else
            {
                return RedirectToAction("IniciarSesion");
            }
        }

        [HttpPost]
        public ActionResult Editar(Media datos)
        {
            bool error = false;
            try
            {   /*
                if (ModelState.IsValid)
                {
                    
                }
                else
                {
                    error = true;
                }*/
                if (!mediaModel.EditarMedia(datos))
                {
                    error = true;
                }
            }
            catch (Exception e)
            {
                System.Diagnostics.Debug.WriteLine(e);
                error = true;
            }

            if (error)
            {
                return RedirectToAction("Error");
            }
            else
            {
                return RedirectToAction("PanelDeControl");
            }
        }
        
        public ActionResult Borrar(int id)
        {
            if (Session["username"] != null)
            {
                Media mediaDB = mediaModel.GetMedia(id);
                if(mediaModel.DeleteMedia(mediaDB))
                {
                    
                    if ((System.IO.File.Exists("~/Recursos/uploads/Ilustraciones/" + mediaDB.Ubicacion)))
                    {
                        System.IO.File.Delete("~/Recursos/uploads/Ilustraciones/" + mediaDB.Ubicacion);
                    }
                    else if ((System.IO.File.Exists("~/Recursos/uploads/Animaciones/" + mediaDB.Ubicacion)))
                    {
                        System.IO.File.Delete("~/Recursos/uploads/Animaciones/" + mediaDB.Ubicacion);
                    }
                    return RedirectToAction("PanelDeControl");
                }
                else
                {
                    return RedirectToAction("Error");
                }
            }
            else
            {
                return RedirectToAction("IniciarSesion");
            }
        }

        public ActionResult Error()
        {
            return View();
        }
    }
}